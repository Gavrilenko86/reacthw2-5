const { createAction } = require("@reduxjs/toolkit");


export const actionProductList = createAction ("ACTIONS_PRODUCT_LIST");
export const actionModalOpen = createAction ("ACTIONS_MODAL_OPEN");
export const actionModalClose = createAction ("ACTIONS_MODAL_CLOSE");
