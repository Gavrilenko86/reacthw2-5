import React, { useState, useEffect } from "react";
import "./Header.scss";
import { Link } from "react-router-dom";


function Header(props) {
    const [cartCount, setCartCount] = useState(props.cartCount);
    const { favoritesCount } = props;

    useEffect(() => {
        // Здесь обновляем cartCount при изменении localStorage
        const savedCart = JSON.parse(localStorage.getItem("cart")) || [];
        setCartCount(savedCart.length);
    }, []);

    return (
        <header>
            <div className="container">
                <Link to="/home" className="logo" ><span>DYSON</span></Link>
                <Link to="/home/cart" className="cart-page"> Cart</Link>
                <Link to="/home/favorites" className="favorites-page">Favorites</Link>
                <div className="content">
                    <div className="cart">
                        <span role="img" aria-label="Cart">🛒</span>: {cartCount}
                    </div>
                    <div className="favorites">
                        <span role="img" aria-label="Favorites">⭐</span>: {favoritesCount}
                    </div>
                </div>
            </div>
        </header>
    );
}

export default Header;
