import React, { useState } from 'react';
import { useDispatch, useSelector } from 'react-redux'; 
import * as actions from "../../store/actions.js"
import PropTypes from 'prop-types';
import Modal from '../Modal/Modal.js';
import './Product.scss';

function Product(props) {
    const showModal = useSelector(state => state.isModalOpen);
    const [isFavorite, setIsFavorite] = useState(props.product.isFavorite || false);
    const dispatch = useDispatch()

    const handleAddToCart = () => {
        dispatch(actions.actionModalOpen());
        props.onAddToCart(props.product);
    };

    const toggleFavorite = () => {
        setIsFavorite(!isFavorite);
        props.onAddToFavorite(props.product.id, !isFavorite);
    };

    return (
        <div className="product-card">
            <img className="img" src={props.product.url} alt={props.product.name} />
            <h3>{props.product.name}</h3>
            <p>Price: ${props.product.price.toFixed(2)} </p>
            <button className="btn-cart" onClick={handleAddToCart}>
                Add to Cart
            </button>
            <button className="btn-favorite" onClick={() => {
                props.addToFavorites(props.product);
                toggleFavorite();
            }}>
                {isFavorite ? '★' : '☆'}
            </button>
            {showModal && (
                <Modal
                    header="Added"
                    text={`${props.product.name} has been added.`}
                    onClose={() => dispatch(actions.actionModalClose())}
                />
            )}
        </div>
    );
}

// Product.propTypes = {
//     product: PropTypes.shape({
//         id: PropTypes.number.isRequired,
//         name: PropTypes.string.isRequired,
//         price: PropTypes.number.isRequired,
//         url: PropTypes.string.isRequired,
//         color: PropTypes.string.isRequired,
//         isFavorite: PropTypes.bool
//     }).isRequired,
//     onAddToCart: PropTypes.func.isRequired,
//     onAddToFavorite: PropTypes.func.isRequired,
//     addToFavorites: PropTypes.func.isRequired
// };

export default Product;
